#ifndef TDBEVENTSSTORAGE_H
#define TDBEVENTSSTORAGE_H

#include <QtSql>
#include <QDateTime>
#include <tbasedbdataset.h>

struct TDBEvent{
    int SourceID;
    QDateTime DateTime;
    short EventTypeID;
    QByteArray Data;
};

class TDBEventsStorage : public TBaseDBDataset
{
public:
    TDBEventsStorage(const QString &host,
                     const QString &username,
                     const QString &password,
                     const int port = 5432);

    bool addEvents(std::vector<TDBEvent> *events);

    QSqlQueryModel* getEventsModel(int id=0,
                                   QDateTime timeFrom = QDateTime::fromString("01012000","ddMMyyyy"),
                                   QDateTime timeTo = QDateTime::currentDateTime(),
                                   int type=0);

    std::vector<TDBEvent> getEvents(int id=0,
                                   QDateTime timeFrom = QDateTime::fromString("01012000","ddMMyyyy"),
                                   QDateTime timeTo = QDateTime::currentDateTime(),
                                   int type=0);

    QSqlQueryModel * getEventsStatic(int id=0,
                                   QDateTime timeFrom = QDateTime::fromString("01012000","ddMMyyyy"),
                                   QDateTime timeTo = QDateTime::currentDateTime(),
                                   int type=0);
};

#endif // TDBEVENTSSTORAGE_H
