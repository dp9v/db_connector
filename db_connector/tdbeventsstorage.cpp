#include "tdbeventsstorage.h"

TDBEventsStorage::TDBEventsStorage(const QString &host,
                                   const QString &username,
                                   const QString &password,
                                   const int port):
    TBaseDBDataset::TBaseDBDataset(host, username, password, "VFDDatabase", port)
{

}

bool TDBEventsStorage::addEvents(std::vector<TDBEvent> *events)
{
    std::vector<TDBEvent>::iterator eventsIterator = events->begin();
    for(;eventsIterator!=events->end();++eventsIterator)
    {
        TDBEvent event = *eventsIterator;
        QString query = QString("SELECT usp_event_insert('%1', %2, '%3');").arg(event.DateTime.toString("yyyy-MM-dd hh:mm:ss"))
                                                                           .arg(QString::number(event.EventTypeID))
                                                                           .arg(QString(event.Data));
        addExecuteQuery(query);
    }
    refreshData();
}


QSqlQueryModel *TDBEventsStorage::getEventsModel(int id, QDateTime timeFrom, QDateTime timeTo, int type)
{
    QString query = QString("SELECT * FROM usp_events_get(%1, '%2', '%3', %4::SMALLINT)").arg(id!=0?QString::number(id):"NULL")
                                                                               .arg(timeFrom.toString("yyyy-MM-dd hh:mm:ss"))
                                                                               .arg(timeTo.toString("yyyy-MM-dd hh:mm:ss"))
                                                                               .arg(type!=0?QString::number(type):"NULL");
    QSqlQueryModel* model = addSelectQuery(query);
    return model;
}

QSqlQueryModel *TDBEventsStorage::getEventsStatic(int id, QDateTime timeFrom, QDateTime timeTo, int type)
{
    QString query = QString("SELECT * FROM usp_events_get(%1, '%2', '%3', %4::SMALLINT)").arg(id!=0?QString::number(id):"NULL")
                                                                               .arg(timeFrom.toString("yyyy-MM-dd hh:mm:ss"))
                                                                               .arg(timeTo.toString("yyyy-MM-dd hh:mm:ss"))
                                                                               .arg(type!=0?QString::number(type):"NULL");
    QSqlQueryModel* model = select(query);
    return model;
}

std::vector<TDBEvent> TDBEventsStorage::getEvents(int id, QDateTime timeFrom, QDateTime timeTo, int type)
{
    QSqlQueryModel* model = getEventsStatic(id, timeFrom, timeTo, type);
    std::vector<TDBEvent> res;
    for(int nRow=0;nRow < model->rowCount();++nRow)
    {
        QSqlRecord sqlRecord = model->record(nRow);
        TDBEvent record;
        record.SourceID = sqlRecord.value("source_id").toInt();
        record.DateTime = QDateTime::fromString(sqlRecord.value("date_time").toString(),"yyyy-MM-dd hh:mm:ss");
        record.EventTypeID = sqlRecord.value("event_type_id").toInt();
        record.Data = sqlRecord.value("data").toByteArray();
        res.push_back(record);
    }
    model->query().clear();
    delete model;
    return res;
}

/*struct TDBEvent{
    int SourceID;
    QDateTime DateTime;
    short EventTypeID;
    QByteArray Data;
};*/
