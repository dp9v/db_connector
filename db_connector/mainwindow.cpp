#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>
#include <mainform.h>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_buttonBox_accepted()
{
    QString host = ui->hostLineEdit->text();
    QString userName = ui->nameLineEdit->text();
    QString pass = ui->passLineEdit->text();
    int port = ui->portLineEdit->text().toInt();

    if(!TBaseDBDataset::checkConnection(host, userName, pass, port))
    {
        QMessageBox::information(this,"Ошбика","Ошибка подключения к базе данных");
    }
    else
    {
        MainForm *form = new MainForm(0, host, userName, pass, port);
        form->show();
        this->close();
    }
}

void MainWindow::on_buttonBox_rejected()
{
    this->close();
}
