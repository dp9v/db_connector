#ifndef TBASEDBDATASET_H
#define TBASEDBDATASET_H

#include <QtSql>;

class TBaseDBDataset
{
protected:
    QString _HOST, _USERNAME, _PASS, _DBNAME;
    int _PORT;

    std::vector<QSqlQuery*> queryVector;
    std::vector<QSqlQueryModel*> queryModelVector;

    QSqlQueryModel * addSelectQuery(const QString query);
    QSqlQueryModel * select(const QString query);
    bool addExecuteQuery(const QString queryText);
    bool execute(const QString queryText);


public:
    TBaseDBDataset(const QString &host,
                   const QString &username,
                   const QString &password,
                   const QString &dbName,
                   const int port = 5432);
    ~TBaseDBDataset();

    void freeQueriesAndModels();
    void freeQueries();
    void freeModels();
    bool refreshData();

    static bool checkConnection(QString &host, QString &username, QString &pass, int port);

private:
    QSqlDatabase database;

    bool connect();
    bool disconnect();


    bool openTransaction();
    bool rollbackTransaction();
    bool commitTransaction();
};

#endif // TBASEDBDATASET_H
