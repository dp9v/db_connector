#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <tbasedbdataset.h>
#include <QSql>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:

    void on_buttonBox_accepted();

    void on_buttonBox_rejected();

private:
    Ui::MainWindow *ui;

    /*const QString HOST = "188.168.20.134";
    const QString USER = "postgres";
    const QString PASS = "pass";*/
};

#endif // MAINWINDOW_H
