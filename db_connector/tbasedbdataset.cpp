#include "tbasedbdataset.h"
#include <QtSql>

TBaseDBDataset::TBaseDBDataset(const QString &host,
                               const QString &username,
                               const QString &password,
                               const QString &dbName,
                               const int port)
{
    TBaseDBDataset::_HOST = host;
    TBaseDBDataset::_USERNAME = username;
    TBaseDBDataset::_PASS = password;
    TBaseDBDataset::_DBNAME = dbName;
    TBaseDBDataset::_PORT = port;
    connect();
}

bool TBaseDBDataset::connect()
{
    database = QSqlDatabase::addDatabase("QPSQL", QString::number(QDateTime::currentMSecsSinceEpoch()));
    database.setHostName(_HOST);
    database.setUserName(_USERNAME);
    database.setPassword(_PASS);
    database.setPort(_PORT);
    database.setDatabaseName(_DBNAME);
    if(!database.open())
    {
        qDebug()<<"Cannot open connection"<<database.lastError();
        return false;
    }
    return true;
}

bool TBaseDBDataset::disconnect()
{
    rollbackTransaction();
    QString connection_name = database.connectionName();
    database.close();
    database.removeDatabase(connection_name);
    return true;
}

bool TBaseDBDataset::openTransaction()
{
    if(!database.transaction())
    {
        qDebug()<<"Cannot open transaction: "<<database.lastError();
        return false;
    }
    return true;
}

bool TBaseDBDataset::commitTransaction()
{
    if(!database.commit())
    {
        qDebug()<<"Cannot commit transaction: "<<database.lastError();
        return false;
    }
    return true;
}

bool TBaseDBDataset::rollbackTransaction()
{
    if(!database.rollback())
    {
        qDebug()<<"Cannot commit transaction: "<<database.lastError();
        return false;
    }
    return true;
}


QSqlQueryModel* TBaseDBDataset::addSelectQuery(const QString query)
{
    QSqlQueryModel *model = new QSqlQueryModel();
    model->setQuery(query, database);
    queryModelVector.push_back(model);
    return model;
}

QSqlQueryModel * TBaseDBDataset::select(const QString query)
{
    QSqlQueryModel *model = new QSqlQueryModel();
    model->setQuery(query, database);
    return model;
}

bool TBaseDBDataset::execute(const QString queryText)
{
    QSqlQuery *sqlQuery = new QSqlQuery(database);
    sqlQuery->prepare(queryText);
    if(!sqlQuery->exec())
    {
        qDebug()<<"Unable to execute query: "<<sqlQuery->lastError();
        return false;
    }
    return true;
}

bool TBaseDBDataset::addExecuteQuery(const QString queryText)
{
    QSqlQuery *query = new QSqlQuery(database);
    query->prepare(queryText);
    queryVector.push_back(query);
    return true;
}

bool TBaseDBDataset::refreshData()
{
    openTransaction();
    std::vector<QSqlQuery*>::iterator queryIterator =  queryVector.begin();
    for(;queryIterator != queryVector.end(); ++queryIterator)
    {
        QSqlQuery *sqlQuery = *queryIterator;
        if(!sqlQuery->exec())
        {
            qDebug()<<"Unable to execute query: "<<sqlQuery->lastError();
            rollbackTransaction();
            return false;
        }
    }
    commitTransaction();
    freeQueries();

    std::vector<QSqlQueryModel*>::iterator modelIterator =  queryModelVector.begin();
    for(;modelIterator != queryModelVector.end(); ++modelIterator)
    {
        QSqlQueryModel *sqlModel = *modelIterator;
        sqlModel->setQuery(sqlModel->query().lastQuery(),database);//обновление данных
    }
    return true;
}

void TBaseDBDataset::freeQueriesAndModels()
{
    freeQueries();
    freeModels();
}

void TBaseDBDataset::freeQueries()
{
    std::vector<QSqlQuery*>::iterator queryIterator =  queryVector.begin();
    for(;queryIterator != queryVector.end(); ++queryIterator)
    {
        QSqlQuery *q = *queryIterator;
        q->clear();
        delete q;
    }
    queryVector.clear();
}

void TBaseDBDataset::freeModels()
{
    std::vector<QSqlQueryModel*>::iterator modelIterator =  queryModelVector.begin();
    for(;modelIterator != queryModelVector.end(); ++modelIterator)
    {
        QSqlQueryModel *sqlModel = *modelIterator;
        sqlModel->query().clear();
        delete sqlModel;
    }
    queryModelVector.clear();
}

TBaseDBDataset::~TBaseDBDataset()
{
    freeQueriesAndModels();
    disconnect();
}


bool TBaseDBDataset::checkConnection(QString &host, QString &username, QString &pass, int port)
{
    QSqlDatabase _database = QSqlDatabase::addDatabase("QPSQL");
    _database.setHostName(host);
    _database.setUserName(username);
    _database.setPassword(pass);
    _database.setPort(port);
    if(!_database.open())
    {
        qDebug()<<"Cannot open connection"<<_database.lastError();
        return false;
    }
    _database.close();
    return true;
}
