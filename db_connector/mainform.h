#ifndef MAINFORM_H
#define MAINFORM_H

#include <QWidget>
#include <tdbeventsstorage.h>
#include <tdbeventtypestorage.h>

namespace Ui {
class MainForm;
}

class MainForm : public QWidget
{
    Q_OBJECT

public:
    explicit MainForm(QWidget *parent = 0,
                      const QString &host="",
                      const QString &username="",
                      const QString &password="",
                      const int port = 5432);
    ~MainForm();

private slots:
    void on_typeAddButton_clicked();

    void on_eventAddButton_clicked();

    void on_filterAddButton_clicked();

    void on_pushButton_3_clicked();

private:
    Ui::MainForm *ui;

    TDBEventTypeStorage *typesStorage;
    TDBEventsStorage *eventsStorage;

    void setComboBoxes();
    void clear();

};

#endif // MAINFORM_H
