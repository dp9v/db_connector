#include "tdbeventtypestorage.h"


TDBEventTypeStorage::TDBEventTypeStorage(const QString &host,
                                       const QString &username,
                                       const QString &password,
                                       const int port):
    TBaseDBDataset(host, username, password, "VFDDatabase", port)
{

}


bool TDBEventTypeStorage::addEventTypes(std::vector<QString> *types)
{
    std::vector<QString>::iterator typeIterator = types->begin();
    for(;typeIterator!=types->end();typeIterator++)
    {
        if(*typeIterator=="")
            continue;
        QString queryText = "SELECT usp_event_type_insert('"+ *typeIterator +"');";
        TDBEventTypeStorage::addExecuteQuery(queryText);
    }
    TDBEventTypeStorage::refreshData();
    return true;
}

QSqlQueryModel* TDBEventTypeStorage::getEventTypesModel()
{
    QString queryText = "SELECT * FROM usp_event_type_get();";
    QSqlQueryModel *model = addSelectQuery(queryText);
    return model;
}

QSqlQueryModel* TDBEventTypeStorage::getEventTypesStatic()
{
    QString queryText = "SELECT * FROM usp_event_type_get();";
    QSqlQueryModel *model = select(queryText);
    return model;
}

std::vector<TDBEventType> TDBEventTypeStorage::getEventTypes()
{
    QSqlQueryModel *model = getEventTypesStatic();
    std::vector<TDBEventType> res;
    for(int nRow=0;nRow < model->rowCount();++nRow)
    {
        QSqlRecord sqlRecord = model->record(nRow);

        TDBEventType record;
        record.event_type_id = sqlRecord.value("event_type_id").toInt();
        record.event_type_description = sqlRecord.value("event_type_description").toByteArray();
        res.push_back(record);
    }
    delete model;
    return res;
}
