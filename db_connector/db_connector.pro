#-------------------------------------------------
#
# Project created by QtCreator 2015-08-07T10:15:03
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = db_connector
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    tbasedbdataset.cpp \
    tdbeventtypestorage.cpp \
    tdbeventsstorage.cpp \
    mainform.cpp

HEADERS  += mainwindow.h \
    tbasedbdataset.h \
    tdbeventtypestorage.h \
    tdbeventsstorage.h \
    mainform.h

FORMS    += mainwindow.ui \
    mainform.ui
