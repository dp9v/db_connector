#ifndef TDBEVENTTYPESTORAGE_H
#define TDBEVENTTYPESTORAGE_H

#include <QtSql>
#include <tbasedbdataset.h>

struct TDBEventType{
    short event_type_id;
    QByteArray event_type_description;
};


class TDBEventTypeStorage : public TBaseDBDataset
{

public:
    TDBEventTypeStorage(const QString &host,
                       const QString &username,
                       const QString &password,
                       const int port = 5432);

    bool addEventTypes(std::vector<QString> *types);

    QSqlQueryModel* getEventTypesModel();
    QSqlQueryModel* getEventTypesStatic();
    std::vector<TDBEventType> getEventTypes();

};

#endif // TDBEVENTTYPESTORAGE_H
