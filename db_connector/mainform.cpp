#include "mainform.h"
#include "ui_mainform.h"
#include <QTableView>

MainForm::MainForm(QWidget *parent,
                   const QString &host,
                   const QString &username,
                   const QString &password,
                   const int port):
    QWidget(parent),
    ui(new Ui::MainForm)
{
    ui->setupUi(this);

    typesStorage = new TDBEventTypeStorage(host, username, password, port);
    eventsStorage = new TDBEventsStorage(host, username, password, port);
    on_pushButton_3_clicked();
}

MainForm::~MainForm()
{
    delete ui;
}

void MainForm::on_typeAddButton_clicked()
{
    std::vector<QString> vec;
    vec.push_back(ui->typeEdit_1->text());
    vec.push_back(ui->typeEdit_2->text());
    vec.push_back(ui->typeEdit_3->text());
    typesStorage->addEventTypes(&vec);
}


void MainForm::on_eventAddButton_clicked()
{
    std::vector<TDBEvent> vec;
    TDBEvent temp;
    temp.DateTime = ui->eventTimeEdit_1->dateTime();
    temp.Data = ui->eventTextEdit_1->toPlainText().toUtf8();
    temp.EventTypeID = ui->eventComboBox_1->model()->index(ui->eventComboBox_1->currentIndex(),0).data().toInt();
    vec.push_back(temp);

    temp.DateTime = ui->eventTimeEdit_2->dateTime();
    temp.Data = ui->eventTextEdit_2->toPlainText().toUtf8();
    temp.EventTypeID = ui->eventComboBox_2->model()->index(ui->eventComboBox_2->currentIndex(),0).data().toInt();
    vec.push_back(temp);

    eventsStorage->addEvents(&vec);
}


void MainForm::on_filterAddButton_clicked()
{
    int fId = ui->filterCheckBox_1->isChecked()? ui->filterIdComboBox->model()->index(ui->filterIdComboBox->currentIndex(),0).data().toInt():0;
    QDateTime timeFrom = ui->filterCheckBox_2->isChecked() ? ui->filterFromDateTimeEdit->dateTime():QDateTime::fromMSecsSinceEpoch(1);
    QDateTime timeTo = ui->filterCheckBox_3->isChecked() ? ui->filterToDateTimeEdit->dateTime():QDateTime::currentDateTime();
    int type = ui->filterCheckBox_4->isChecked()? ui->filterTypeComboBox->model()->index(ui->filterTypeComboBox->currentIndex(),0).data().toInt():0;

    QSqlQueryModel *model = ui->filterForceCheckBox->isChecked() ? eventsStorage->getEventsModel(fId, timeFrom, timeTo, type) : eventsStorage->getEventsStatic(fId, timeFrom, timeTo, type);

    QWidget *newTab = new QWidget();
    QTableView *newTableView = new QTableView(newTab);
    newTableView->setModel(model);
    newTableView->setGeometry(QRect(0, 0, 871, 711));
    ui->tabWidget->addTab(newTab, QString("New filter"));
}

void MainForm::on_pushButton_3_clicked()
{
    eventsStorage->freeQueriesAndModels();
    ui->tabWidget->clear();
    QWidget *newTab = new QWidget();
    QTableView *newTableView = new QTableView(newTab);
    newTableView->setModel(eventsStorage->getEventsModel());
    newTableView->setGeometry(QRect(0, 0, 871, 711));
    ui->tabWidget->addTab(newTab, QString("Main Tab"));
    setComboBoxes();
}

void MainForm::setComboBoxes()
{
    QSqlQueryModel *model = typesStorage->getEventTypesModel();
    ui->eventComboBox_1->setModel(model);
    ui->eventComboBox_1->setModelColumn(1);
    ui->eventComboBox_2->setModel(model);
    ui->eventComboBox_2->setModelColumn(1);
    ui->filterTypeComboBox->setModel(model);
    ui->filterTypeComboBox->setModelColumn(1);
    model = eventsStorage->getEventsModel();
    ui->filterIdComboBox->setModel(model);
    ui->filterIdComboBox->setModelColumn(0);
}
